package com.myappconverter.mobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;
import android.graphics.Typeface;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Color;
import com.myappconverter.mobile.Utils;
import com.myappconverter.mobile.OnSwipeTouchListener;
import java.util.ArrayList;
import java.util.List;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


public class Activity_Vic_8g_ebU extends AppCompatActivity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vic_8g_ebu);
        View view = findViewById(android.R.id.content);
        
		Spinner spinner_Xr7_l1_IlM = (Spinner) view.findViewById(R.id.Xr7_l1_IlM);
		List<String> listspinner_Xr7_l1_IlM = new ArrayList<String>();
		listspinner_Xr7_l1_IlM.add("choose a value");
		listspinner_Xr7_l1_IlM.add("value 1");
		listspinner_Xr7_l1_IlM.add("value 2");
		listspinner_Xr7_l1_IlM.add("value 3");
		ArrayAdapter<String> arrayAdapterspinner_Xr7_l1_IlM = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listspinner_Xr7_l1_IlM);
		arrayAdapterspinner_Xr7_l1_IlM.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner_Xr7_l1_IlM.setAdapter(arrayAdapterspinner_Xr7_l1_IlM);

		// Swipe navigation
		RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.main_Activity_2sa_q5_Eym);
		View viewSwipe = Utils.addSwipeView(relativeLayout, this);
		viewSwipe.setOnTouchListener(new OnSwipeTouchListener(this,viewSwipe));

		View btnLeft = findViewById(R.id.btn_swipe_left);
	    btnLeft.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_cIE_zJ_9nQ.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_right, R.anim.translate_out_right);
	        finish();
	      }
	    });

	    View btnRight = findViewById(R.id.btn_swipe_right);
	    btnRight.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_4ep_Np_ax8.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_left, R.anim.translate_out_left);
	        finish();
	      }
	    });

		TextView titleBackBarButton = (TextView) view.findViewById(R.id.nav_item_back_title);
        if(titleBackBarButton!=null) {
            titleBackBarButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }


	}

}