package com.myappconverter.mobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;
import android.graphics.Typeface;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Color;
import com.myappconverter.mobile.Utils;
import com.myappconverter.mobile.OnSwipeTouchListener;
import java.util.ArrayList;
import java.util.List;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.myappconverter.mobile.adapter.BaseAdapter_id_1Vw_q6_c3A;


public class Activity_S7v_gD_IxW extends AppCompatActivity implements CallBack{

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s7v_gd_ixw);
        View view = findViewById(android.R.id.content);
        
		ListView listView_id_1Vw_q6_c3A = (ListView) view.findViewById(R.id.id_1Vw_q6_c3A);
		listView_id_1Vw_q6_c3A.setAdapter(new BaseAdapter_id_1Vw_q6_c3A(this));

		// Swipe navigation
		RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.main_Activity_9Dm_D2_Zsy);
		View viewSwipe = Utils.addSwipeView(relativeLayout, this);
		viewSwipe.setOnTouchListener(new OnSwipeTouchListener(this,viewSwipe));

		View btnLeft = findViewById(R.id.btn_swipe_left);
	    btnLeft.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_4ep_Np_ax8.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_right, R.anim.translate_out_right);
	        finish();
	      }
	    });

	    View btnRight = findViewById(R.id.btn_swipe_right);
	    btnRight.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_cIE_zJ_9nQ.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_left, R.anim.translate_out_left);
	        finish();
	      }
	    });

		TextView titleBackBarButton = (TextView) view.findViewById(R.id.nav_item_back_title);
        if(titleBackBarButton!=null) {
            titleBackBarButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }


	}	@Override
	public void perform(int position){
		switch (position){
			case 0:
					
			startActivity(new Intent(getApplicationContext(),Activity_4ep_Np_ax8.class));
			break;

		}
	}

}