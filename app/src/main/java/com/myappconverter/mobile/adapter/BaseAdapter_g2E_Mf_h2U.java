package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_g2E_Mf_h2U extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
		public BaseAdapter_g2E_Mf_h2U(CallBack callBack){mCallBack = callBack;}

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_cell_zwy_uu_krk, null);
                break;

			}

		convertView.setTag(position);
        
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			mCallBack.perform((Integer) v.getTag());
		}});
        return convertView;
    }
}
