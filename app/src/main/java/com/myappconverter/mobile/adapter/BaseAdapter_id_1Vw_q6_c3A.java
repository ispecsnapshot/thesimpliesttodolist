package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_id_1Vw_q6_c3A extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
		public BaseAdapter_id_1Vw_q6_c3A(CallBack callBack){mCallBack = callBack;}

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_s7v_gd_ixwcell_5tr_pl_2vg, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_s7v_gd_ixwcell_xnn_ie_xbm, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_s7v_gd_ixwcell_jpt_st_qcz, null);
                break;

			}

		convertView.setTag(position);
        
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			mCallBack.perform((Integer) v.getTag());
		}});
        return convertView;
    }
}
